package api

// Code generated by go-bare/cmd/gen, DO NOT EDIT.

import (
	"errors"
	"git.sr.ht/~sircmpwn/go-bare"
)

type LineResponseDev struct {
	Line LineV3 `bare:"line"`
}

func (t *LineResponseDev) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LineResponseDev) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LineResponseV3 struct {
	Line LineV3 `bare:"line"`
}

func (t *LineResponseV3) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LineResponseV3) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LineResponseV2 struct {
	Line LineV2 `bare:"line"`
}

func (t *LineResponseV2) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LineResponseV2) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LineResponseV1 struct {
	Line LineV1 `bare:"line"`
}

func (t *LineResponseV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LineResponseV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type FeedsResponseDev struct {
	Feeds []FeedInfoV2 `bare:"feeds"`
}

func (t *FeedsResponseDev) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *FeedsResponseDev) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type FeedsResponseV2 struct {
	Feeds []FeedInfoV2 `bare:"feeds"`
}

func (t *FeedsResponseV2) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *FeedsResponseV2) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type FeedsResponseV1 struct {
	Feeds []FeedInfoV1 `bare:"feeds"`
}

func (t *FeedsResponseV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *FeedsResponseV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type FeedInfoV1 struct {
	Name        string `bare:"name"`
	Id          string `bare:"id"`
	Attribution string `bare:"attribution"`
	Description string `bare:"description"`
	LastUpdate  string `bare:"lastUpdate"`
}

func (t *FeedInfoV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *FeedInfoV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type FeedInfoV2 struct {
	Name        string       `bare:"name"`
	Id          string       `bare:"id"`
	Attribution string       `bare:"attribution"`
	Description string       `bare:"description"`
	LastUpdate  string       `bare:"lastUpdate"`
	QrHost      string       `bare:"qrHost"`
	QrIn        QRLocationV1 `bare:"qrIn"`
	QrSelector  string       `bare:"qrSelector"`
	ValidSince  string       `bare:"validSince"`
	ValidTill   string       `bare:"validTill"`
}

func (t *FeedInfoV2) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *FeedInfoV2) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type QueryablesResponseDev struct {
	Queryables []QueryableV4 `bare:"queryables"`
}

func (t *QueryablesResponseDev) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *QueryablesResponseDev) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type QueryablesResponseV4 struct {
	Queryables []QueryableV4 `bare:"queryables"`
}

func (t *QueryablesResponseV4) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *QueryablesResponseV4) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type QueryablesResponseV3 struct {
	Queryables []QueryableV3 `bare:"queryables"`
}

func (t *QueryablesResponseV3) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *QueryablesResponseV3) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type QueryablesResponseV2 struct {
	Queryables []QueryableV2 `bare:"queryables"`
}

func (t *QueryablesResponseV2) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *QueryablesResponseV2) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type QueryablesResponseV1 struct {
	Queryables []QueryableV1 `bare:"queryables"`
}

func (t *QueryablesResponseV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *QueryablesResponseV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type StopV1 struct {
	Code          string           `bare:"code"`
	Name          string           `bare:"name"`
	Zone          string           `bare:"zone"`
	Position      PositionV1       `bare:"position"`
	ChangeOptions []ChangeOptionV1 `bare:"changeOptions"`
}

func (t *StopV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *StopV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type StopV2 struct {
	Code          string           `bare:"code"`
	Name          string           `bare:"name"`
	NodeName      string           `bare:"nodeName"`
	Zone          string           `bare:"zone"`
	FeedID        string           `bare:"feedID"`
	Position      PositionV1       `bare:"position"`
	ChangeOptions []ChangeOptionV1 `bare:"changeOptions"`
}

func (t *StopV2) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *StopV2) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LineV1 struct {
	Name      string        `bare:"name"`
	Colour    ColourV1      `bare:"colour"`
	Kind      LineTypeV2    `bare:"kind"`
	FeedID    string        `bare:"feedID"`
	Headsigns [][]string    `bare:"headsigns"`
	Graphs    []LineGraphV1 `bare:"graphs"`
}

func (t *LineV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LineV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LineV2 struct {
	Name      string        `bare:"name"`
	Colour    ColourV1      `bare:"colour"`
	Kind      LineTypeV3    `bare:"kind"`
	FeedID    string        `bare:"feedID"`
	Headsigns [][]string    `bare:"headsigns"`
	Graphs    []LineGraphV1 `bare:"graphs"`
}

func (t *LineV2) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LineV2) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LineV3 struct {
	Id        string        `bare:"id"`
	Name      string        `bare:"name"`
	Colour    ColourV1      `bare:"colour"`
	Kind      LineTypeV3    `bare:"kind"`
	FeedID    string        `bare:"feedID"`
	Headsigns [][]string    `bare:"headsigns"`
	Graphs    []LineGraphV1 `bare:"graphs"`
}

func (t *LineV3) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LineV3) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LineGraphV1 struct {
	Stops     []StopStubV1  `bare:"stops"`
	NextNodes map[int][]int `bare:"nextNodes"`
}

func (t *LineGraphV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LineGraphV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type StopStubV1 struct {
	Code     string `bare:"code"`
	Name     string `bare:"name"`
	NodeName string `bare:"nodeName"`
	Zone     string `bare:"zone"`
	OnDemand bool   `bare:"onDemand"`
}

func (t *StopStubV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *StopStubV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type PositionV1 struct {
	Lat float64 `bare:"lat"`
	Lon float64 `bare:"lon"`
}

func (t *PositionV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *PositionV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type ChangeOptionV1 struct {
	LineName string `bare:"lineName"`
	Headsign string `bare:"headsign"`
}

func (t *ChangeOptionV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *ChangeOptionV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LocatablesResponseDev struct {
	Locatables []LocatableV3 `bare:"locatables"`
}

func (t *LocatablesResponseDev) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LocatablesResponseDev) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LocatablesResponseV3 struct {
	Locatables []LocatableV3 `bare:"locatables"`
}

func (t *LocatablesResponseV3) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LocatablesResponseV3) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LocatablesResponseV2 struct {
	Locatables []LocatableV2 `bare:"locatables"`
}

func (t *LocatablesResponseV2) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LocatablesResponseV2) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LocatablesResponseV1 struct {
	Locatables []LocatableV1 `bare:"locatables"`
}

func (t *LocatablesResponseV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LocatablesResponseV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type VehicleV1 struct {
	Id              string            `bare:"id"`
	Position        PositionV1        `bare:"position"`
	Capabilities    uint16            `bare:"capabilities"`
	Speed           float32           `bare:"speed"`
	Line            LineStubV1        `bare:"line"`
	Headsign        string            `bare:"headsign"`
	CongestionLevel CongestionLevelV1 `bare:"congestionLevel"`
	OccupancyStatus OccupancyStatusV1 `bare:"occupancyStatus"`
}

func (t *VehicleV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *VehicleV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type VehicleV2 struct {
	Id              string            `bare:"id"`
	Position        PositionV1        `bare:"position"`
	Capabilities    uint16            `bare:"capabilities"`
	Speed           float32           `bare:"speed"`
	Line            LineStubV2        `bare:"line"`
	Headsign        string            `bare:"headsign"`
	CongestionLevel CongestionLevelV1 `bare:"congestionLevel"`
	OccupancyStatus OccupancyStatusV1 `bare:"occupancyStatus"`
}

func (t *VehicleV2) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *VehicleV2) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type VehicleV3 struct {
	Id              string            `bare:"id"`
	Position        PositionV1        `bare:"position"`
	Capabilities    uint16            `bare:"capabilities"`
	Speed           float32           `bare:"speed"`
	Line            LineStubV3        `bare:"line"`
	Headsign        string            `bare:"headsign"`
	CongestionLevel CongestionLevelV1 `bare:"congestionLevel"`
	OccupancyStatus OccupancyStatusV1 `bare:"occupancyStatus"`
}

func (t *VehicleV3) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *VehicleV3) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LineStubV1 struct {
	Name   string     `bare:"name"`
	Kind   LineTypeV1 `bare:"kind"`
	Colour ColourV1   `bare:"colour"`
}

func (t *LineStubV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LineStubV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LineStubV2 struct {
	Name   string     `bare:"name"`
	Kind   LineTypeV2 `bare:"kind"`
	Colour ColourV1   `bare:"colour"`
}

func (t *LineStubV2) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LineStubV2) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type LineStubV3 struct {
	Name   string     `bare:"name"`
	Kind   LineTypeV3 `bare:"kind"`
	Colour ColourV1   `bare:"colour"`
}

func (t *LineStubV3) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *LineStubV3) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type ColourV1 struct {
	R uint8 `bare:"r"`
	G uint8 `bare:"g"`
	B uint8 `bare:"b"`
}

func (t *ColourV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *ColourV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type DeparturesResponseDev struct {
	Alerts     []AlertV1     `bare:"alerts"`
	Departures []DepartureV4 `bare:"departures"`
	Stop       StopV2        `bare:"stop"`
}

func (t *DeparturesResponseDev) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *DeparturesResponseDev) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type DeparturesResponseV4 struct {
	Alerts     []AlertV1     `bare:"alerts"`
	Departures []DepartureV4 `bare:"departures"`
	Stop       StopV2        `bare:"stop"`
}

func (t *DeparturesResponseV4) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *DeparturesResponseV4) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type DeparturesResponseV3 struct {
	Alerts     []AlertV1     `bare:"alerts"`
	Departures []DepartureV3 `bare:"departures"`
	Stop       StopV2        `bare:"stop"`
}

func (t *DeparturesResponseV3) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *DeparturesResponseV3) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type DeparturesResponseV2 struct {
	Alerts     []AlertV1     `bare:"alerts"`
	Departures []DepartureV2 `bare:"departures"`
	Stop       StopV2        `bare:"stop"`
}

func (t *DeparturesResponseV2) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *DeparturesResponseV2) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type DeparturesResponseV1 struct {
	Alerts     []AlertV1     `bare:"alerts"`
	Departures []DepartureV1 `bare:"departures"`
	Stop       StopV1        `bare:"stop"`
}

func (t *DeparturesResponseV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *DeparturesResponseV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type AlertV1 struct {
	Header      string        `bare:"header"`
	Description string        `bare:"description"`
	Url         string        `bare:"url"`
	Cause       AlertCauseV1  `bare:"cause"`
	Effect      AlertEffectV1 `bare:"effect"`
}

func (t *AlertV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *AlertV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type DepartureV1 struct {
	Id         string          `bare:"id"`
	Time       TimeV1          `bare:"time"`
	Status     VehicleStatusV1 `bare:"status"`
	IsRealtime bool            `bare:"isRealtime"`
	Vehicle    VehicleV1       `bare:"vehicle"`
	Boarding   uint8           `bare:"boarding"`
}

func (t *DepartureV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *DepartureV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type DepartureV2 struct {
	Id         string          `bare:"id"`
	Time       TimeV1          `bare:"time"`
	Status     VehicleStatusV1 `bare:"status"`
	IsRealtime bool            `bare:"isRealtime"`
	Vehicle    VehicleV2       `bare:"vehicle"`
	Boarding   uint8           `bare:"boarding"`
}

func (t *DepartureV2) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *DepartureV2) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type DepartureV3 struct {
	Id         string          `bare:"id"`
	Time       TimeV1          `bare:"time"`
	Status     VehicleStatusV1 `bare:"status"`
	IsRealtime bool            `bare:"isRealtime"`
	Vehicle    VehicleV3       `bare:"vehicle"`
	Boarding   uint8           `bare:"boarding"`
}

func (t *DepartureV3) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *DepartureV3) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type DepartureV4 struct {
	Id         string          `bare:"id"`
	Time       TimeV1          `bare:"time"`
	Status     VehicleStatusV1 `bare:"status"`
	IsRealtime bool            `bare:"isRealtime"`
	Vehicle    VehicleV3       `bare:"vehicle"`
	Boarding   uint8           `bare:"boarding"`
	Alerts     []AlertV1       `bare:"alerts"`
}

func (t *DepartureV4) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *DepartureV4) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type TimeV1 struct {
	Hour      uint8  `bare:"hour"`
	Minute    uint8  `bare:"minute"`
	Second    uint8  `bare:"second"`
	DayOffset int8   `bare:"dayOffset"`
	Zone      string `bare:"zone"`
}

func (t *TimeV1) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *TimeV1) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type ErrorResponse struct {
	Field   string `bare:"field"`
	Message string `bare:"message"`
}

func (t *ErrorResponse) Decode(data []byte) error {
	return bare.Unmarshal(data, t)
}

func (t *ErrorResponse) Encode() ([]byte, error) {
	return bare.Marshal(t)
}

type QRLocationV1 uint

const (
	UNKNOWN QRLocationV1 = 0
	NONE    QRLocationV1 = 1
	PATH    QRLocationV1 = 2
	QUERY   QRLocationV1 = 3
)

func (t QRLocationV1) String() string {
	switch t {
	case UNKNOWN:
		return "UNKNOWN"
	case NONE:
		return "NONE"
	case PATH:
		return "PATH"
	case QUERY:
		return "QUERY"
	}
	panic(errors.New("Invalid QRLocationV1 value"))
}

type CongestionLevelV1 uint

const (
	CONGESTION_UNKNOWN     CongestionLevelV1 = 0
	CONGESTION_SMOOTH      CongestionLevelV1 = 1
	CONGESTION_STOP_AND_GO CongestionLevelV1 = 2
	CONGESTION_SIGNIFICANT CongestionLevelV1 = 3
	CONGESTION_SEVERE      CongestionLevelV1 = 4
)

func (t CongestionLevelV1) String() string {
	switch t {
	case CONGESTION_UNKNOWN:
		return "CONGESTION_UNKNOWN"
	case CONGESTION_SMOOTH:
		return "CONGESTION_SMOOTH"
	case CONGESTION_STOP_AND_GO:
		return "CONGESTION_STOP_AND_GO"
	case CONGESTION_SIGNIFICANT:
		return "CONGESTION_SIGNIFICANT"
	case CONGESTION_SEVERE:
		return "CONGESTION_SEVERE"
	}
	panic(errors.New("Invalid CongestionLevelV1 value"))
}

type OccupancyStatusV1 uint

const (
	OCCUPANCY_UNKNOWN        OccupancyStatusV1 = 0
	OCCUPANCY_EMPTY          OccupancyStatusV1 = 1
	OCCUPANCY_MANY_AVAILABLE OccupancyStatusV1 = 2
	OCCUPANCY_FEW_AVAILABLE  OccupancyStatusV1 = 3
	OCCUPANCY_STANDING_ONLY  OccupancyStatusV1 = 4
	OCCUPANCY_CRUSHED        OccupancyStatusV1 = 5
	OCCUPANCY_FULL           OccupancyStatusV1 = 6
	OCCUPANCY_NOT_ACCEPTING  OccupancyStatusV1 = 7
)

func (t OccupancyStatusV1) String() string {
	switch t {
	case OCCUPANCY_UNKNOWN:
		return "OCCUPANCY_UNKNOWN"
	case OCCUPANCY_EMPTY:
		return "OCCUPANCY_EMPTY"
	case OCCUPANCY_MANY_AVAILABLE:
		return "OCCUPANCY_MANY_AVAILABLE"
	case OCCUPANCY_FEW_AVAILABLE:
		return "OCCUPANCY_FEW_AVAILABLE"
	case OCCUPANCY_STANDING_ONLY:
		return "OCCUPANCY_STANDING_ONLY"
	case OCCUPANCY_CRUSHED:
		return "OCCUPANCY_CRUSHED"
	case OCCUPANCY_FULL:
		return "OCCUPANCY_FULL"
	case OCCUPANCY_NOT_ACCEPTING:
		return "OCCUPANCY_NOT_ACCEPTING"
	}
	panic(errors.New("Invalid OccupancyStatusV1 value"))
}

type LineTypeV1 uint

const (
	LINE_UNKNOWN LineTypeV1 = 0
	TRAM         LineTypeV1 = 1
	BUS          LineTypeV1 = 2
)

func (t LineTypeV1) String() string {
	switch t {
	case LINE_UNKNOWN:
		return "LINE_UNKNOWN"
	case TRAM:
		return "TRAM"
	case BUS:
		return "BUS"
	}
	panic(errors.New("Invalid LineTypeV1 value"))
}

type LineTypeV2 uint

const (
	LINE_V2_UNKNOWN    LineTypeV2 = 0
	LINE_V2_TRAM       LineTypeV2 = 1
	LINE_V2_BUS        LineTypeV2 = 2
	LINE_V2_TROLLEYBUS LineTypeV2 = 3
)

func (t LineTypeV2) String() string {
	switch t {
	case LINE_V2_UNKNOWN:
		return "LINE_V2_UNKNOWN"
	case LINE_V2_TRAM:
		return "LINE_V2_TRAM"
	case LINE_V2_BUS:
		return "LINE_V2_BUS"
	case LINE_V2_TROLLEYBUS:
		return "LINE_V2_TROLLEYBUS"
	}
	panic(errors.New("Invalid LineTypeV2 value"))
}

type LineTypeV3 uint

const (
	LINE_V3_UNKNOWN    LineTypeV3 = 0
	LINE_V3_TRAM       LineTypeV3 = 1
	LINE_V3_BUS        LineTypeV3 = 2
	LINE_V3_TROLLEYBUS LineTypeV3 = 3
	LINE_V3_METRO      LineTypeV3 = 4
	LINE_V3_RAIL       LineTypeV3 = 5
	LINE_V3_FERRY      LineTypeV3 = 6
	LINE_V3_CABLE_TRAM LineTypeV3 = 7
	LINE_V3_CABLE_CAR  LineTypeV3 = 8
	LINE_V3_FUNICULAR  LineTypeV3 = 9
	LINE_V3_MONORAIL   LineTypeV3 = 10
)

func (t LineTypeV3) String() string {
	switch t {
	case LINE_V3_UNKNOWN:
		return "LINE_V3_UNKNOWN"
	case LINE_V3_TRAM:
		return "LINE_V3_TRAM"
	case LINE_V3_BUS:
		return "LINE_V3_BUS"
	case LINE_V3_TROLLEYBUS:
		return "LINE_V3_TROLLEYBUS"
	case LINE_V3_METRO:
		return "LINE_V3_METRO"
	case LINE_V3_RAIL:
		return "LINE_V3_RAIL"
	case LINE_V3_FERRY:
		return "LINE_V3_FERRY"
	case LINE_V3_CABLE_TRAM:
		return "LINE_V3_CABLE_TRAM"
	case LINE_V3_CABLE_CAR:
		return "LINE_V3_CABLE_CAR"
	case LINE_V3_FUNICULAR:
		return "LINE_V3_FUNICULAR"
	case LINE_V3_MONORAIL:
		return "LINE_V3_MONORAIL"
	}
	panic(errors.New("Invalid LineTypeV3 value"))
}

type AlertCauseV1 uint

const (
	CAUSE_UNKNOWN           AlertCauseV1 = 0
	CAUSE_OTHER             AlertCauseV1 = 1
	CAUSE_TECHNICAL_PROBLEM AlertCauseV1 = 2
	CAUSE_STRIKE            AlertCauseV1 = 3
	CAUSE_DEMONSTRATION     AlertCauseV1 = 4
	CAUSE_ACCIDENT          AlertCauseV1 = 5
	CAUSE_HOLIDAY           AlertCauseV1 = 6
	CAUSE_WEATHER           AlertCauseV1 = 7
	CAUSE_MAINTENANCE       AlertCauseV1 = 8
	CAUSE_CONSTRUCTION      AlertCauseV1 = 9
	CAUSE_POLICE_ACTIVITY   AlertCauseV1 = 10
	CAUSE_MEDICAL_EMERGENCY AlertCauseV1 = 11
)

func (t AlertCauseV1) String() string {
	switch t {
	case CAUSE_UNKNOWN:
		return "CAUSE_UNKNOWN"
	case CAUSE_OTHER:
		return "CAUSE_OTHER"
	case CAUSE_TECHNICAL_PROBLEM:
		return "CAUSE_TECHNICAL_PROBLEM"
	case CAUSE_STRIKE:
		return "CAUSE_STRIKE"
	case CAUSE_DEMONSTRATION:
		return "CAUSE_DEMONSTRATION"
	case CAUSE_ACCIDENT:
		return "CAUSE_ACCIDENT"
	case CAUSE_HOLIDAY:
		return "CAUSE_HOLIDAY"
	case CAUSE_WEATHER:
		return "CAUSE_WEATHER"
	case CAUSE_MAINTENANCE:
		return "CAUSE_MAINTENANCE"
	case CAUSE_CONSTRUCTION:
		return "CAUSE_CONSTRUCTION"
	case CAUSE_POLICE_ACTIVITY:
		return "CAUSE_POLICE_ACTIVITY"
	case CAUSE_MEDICAL_EMERGENCY:
		return "CAUSE_MEDICAL_EMERGENCY"
	}
	panic(errors.New("Invalid AlertCauseV1 value"))
}

type AlertEffectV1 uint

const (
	EFFECT_UNKNOWN             AlertEffectV1 = 0
	EFFECT_OTHER               AlertEffectV1 = 1
	EFFECT_NO_SERVICE          AlertEffectV1 = 2
	EFFECT_REDUCED_SERVICE     AlertEffectV1 = 3
	EFFECT_SIGNIFICANT_DELAYS  AlertEffectV1 = 4
	EFFECT_DETOUR              AlertEffectV1 = 5
	EFFECT_ADDITIONAL_SERVICE  AlertEffectV1 = 6
	EFFECT_MODIFIED_SERVICE    AlertEffectV1 = 7
	EFFECT_STOP_MOVED          AlertEffectV1 = 8
	EFFECT_NONE                AlertEffectV1 = 9
	EFFECT_ACCESSIBILITY_ISSUE AlertEffectV1 = 10
)

func (t AlertEffectV1) String() string {
	switch t {
	case EFFECT_UNKNOWN:
		return "EFFECT_UNKNOWN"
	case EFFECT_OTHER:
		return "EFFECT_OTHER"
	case EFFECT_NO_SERVICE:
		return "EFFECT_NO_SERVICE"
	case EFFECT_REDUCED_SERVICE:
		return "EFFECT_REDUCED_SERVICE"
	case EFFECT_SIGNIFICANT_DELAYS:
		return "EFFECT_SIGNIFICANT_DELAYS"
	case EFFECT_DETOUR:
		return "EFFECT_DETOUR"
	case EFFECT_ADDITIONAL_SERVICE:
		return "EFFECT_ADDITIONAL_SERVICE"
	case EFFECT_MODIFIED_SERVICE:
		return "EFFECT_MODIFIED_SERVICE"
	case EFFECT_STOP_MOVED:
		return "EFFECT_STOP_MOVED"
	case EFFECT_NONE:
		return "EFFECT_NONE"
	case EFFECT_ACCESSIBILITY_ISSUE:
		return "EFFECT_ACCESSIBILITY_ISSUE"
	}
	panic(errors.New("Invalid AlertEffectV1 value"))
}

type VehicleStatusV1 uint

const (
	STATUS_IN_TRANSIT VehicleStatusV1 = 0
	STATUS_INCOMING   VehicleStatusV1 = 1
	STATUS_AT_STOP    VehicleStatusV1 = 2
	STATUS_DEPARTED   VehicleStatusV1 = 3
)

func (t VehicleStatusV1) String() string {
	switch t {
	case STATUS_IN_TRANSIT:
		return "STATUS_IN_TRANSIT"
	case STATUS_INCOMING:
		return "STATUS_INCOMING"
	case STATUS_AT_STOP:
		return "STATUS_AT_STOP"
	case STATUS_DEPARTED:
		return "STATUS_DEPARTED"
	}
	panic(errors.New("Invalid VehicleStatusV1 value"))
}

type BoardingV1 uint

const (
	BOARDING_NONE       BoardingV1 = 0
	ONBOARDING_REGULAR  BoardingV1 = 1
	ONBOARDING_PHONE    BoardingV1 = 2
	ONBOARDING_DRIVER   BoardingV1 = 3
	OFFBOARDING_REGULAR BoardingV1 = 16
	OFFBOARDING_PHONE   BoardingV1 = 32
	OFFBOARDING_DRIVER  BoardingV1 = 48
)

func (t BoardingV1) String() string {
	switch t {
	case BOARDING_NONE:
		return "BOARDING_NONE"
	case ONBOARDING_REGULAR:
		return "ONBOARDING_REGULAR"
	case ONBOARDING_PHONE:
		return "ONBOARDING_PHONE"
	case ONBOARDING_DRIVER:
		return "ONBOARDING_DRIVER"
	case OFFBOARDING_REGULAR:
		return "OFFBOARDING_REGULAR"
	case OFFBOARDING_PHONE:
		return "OFFBOARDING_PHONE"
	case OFFBOARDING_DRIVER:
		return "OFFBOARDING_DRIVER"
	}
	panic(errors.New("Invalid BoardingV1 value"))
}

type LineResponse interface {
	bare.Union
}

func (_ LineResponseDev) IsUnion() {}

func (_ LineResponseV1) IsUnion() {}

func (_ LineResponseV2) IsUnion() {}

func (_ LineResponseV3) IsUnion() {}

type FeedsResponse interface {
	bare.Union
}

func (_ FeedsResponseDev) IsUnion() {}

func (_ FeedsResponseV1) IsUnion() {}

func (_ FeedsResponseV2) IsUnion() {}

type QueryablesResponse interface {
	bare.Union
}

func (_ QueryablesResponseDev) IsUnion() {}

func (_ QueryablesResponseV1) IsUnion() {}

func (_ QueryablesResponseV2) IsUnion() {}

func (_ QueryablesResponseV3) IsUnion() {}

func (_ QueryablesResponseV4) IsUnion() {}

type QueryableV1 interface {
	bare.Union
}

func (_ StopV1) IsUnion() {}

type QueryableV2 interface {
	bare.Union
}

func (_ StopV2) IsUnion() {}

func (_ LineV1) IsUnion() {}

type QueryableV3 interface {
	bare.Union
}


func (_ LineV2) IsUnion() {}

type QueryableV4 interface {
	bare.Union
}


func (_ LineV3) IsUnion() {}

type LocatablesResponse interface {
	bare.Union
}

func (_ LocatablesResponseDev) IsUnion() {}

func (_ LocatablesResponseV1) IsUnion() {}

func (_ LocatablesResponseV2) IsUnion() {}

func (_ LocatablesResponseV3) IsUnion() {}

type LocatableV1 interface {
	bare.Union
}


func (_ VehicleV1) IsUnion() {}

type LocatableV2 interface {
	bare.Union
}


func (_ VehicleV2) IsUnion() {}

type LocatableV3 interface {
	bare.Union
}


func (_ VehicleV3) IsUnion() {}

type DeparturesResponse interface {
	bare.Union
}

func (_ DeparturesResponseDev) IsUnion() {}

func (_ DeparturesResponseV1) IsUnion() {}

func (_ DeparturesResponseV2) IsUnion() {}

func (_ DeparturesResponseV3) IsUnion() {}

func (_ DeparturesResponseV4) IsUnion() {}

func init() {
	bare.RegisterUnion((*LineResponse)(nil)).
		Member(*new(LineResponseDev), 0).
		Member(*new(LineResponseV1), 1).
		Member(*new(LineResponseV2), 2).
		Member(*new(LineResponseV3), 3)

	bare.RegisterUnion((*FeedsResponse)(nil)).
		Member(*new(FeedsResponseDev), 0).
		Member(*new(FeedsResponseV1), 1).
		Member(*new(FeedsResponseV2), 2)

	bare.RegisterUnion((*QueryablesResponse)(nil)).
		Member(*new(QueryablesResponseDev), 0).
		Member(*new(QueryablesResponseV1), 1).
		Member(*new(QueryablesResponseV2), 2).
		Member(*new(QueryablesResponseV3), 3).
		Member(*new(QueryablesResponseV4), 4)

	bare.RegisterUnion((*QueryableV1)(nil)).
		Member(*new(StopV1), 0)

	bare.RegisterUnion((*QueryableV2)(nil)).
		Member(*new(StopV2), 0).
		Member(*new(LineV1), 1)

	bare.RegisterUnion((*QueryableV3)(nil)).
		Member(*new(StopV2), 0).
		Member(*new(LineV2), 1)

	bare.RegisterUnion((*QueryableV4)(nil)).
		Member(*new(StopV2), 0).
		Member(*new(LineV3), 1)

	bare.RegisterUnion((*LocatablesResponse)(nil)).
		Member(*new(LocatablesResponseDev), 0).
		Member(*new(LocatablesResponseV1), 1).
		Member(*new(LocatablesResponseV2), 2).
		Member(*new(LocatablesResponseV3), 3)

	bare.RegisterUnion((*LocatableV1)(nil)).
		Member(*new(StopV1), 0).
		Member(*new(VehicleV1), 1)

	bare.RegisterUnion((*LocatableV2)(nil)).
		Member(*new(StopV2), 0).
		Member(*new(VehicleV2), 1)

	bare.RegisterUnion((*LocatableV3)(nil)).
		Member(*new(StopV2), 0).
		Member(*new(VehicleV3), 1)

	bare.RegisterUnion((*DeparturesResponse)(nil)).
		Member(*new(DeparturesResponseDev), 0).
		Member(*new(DeparturesResponseV1), 1).
		Member(*new(DeparturesResponseV2), 2).
		Member(*new(DeparturesResponseV3), 3).
		Member(*new(DeparturesResponseV4), 4)

}
