// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package api

import "github.com/dhconnelly/rtreego"

type TimedStopStub struct {
	// StopStub
	Time uint
}

func (s StopV2) Bounds() *rtreego.Rect {
	rect, err := rtreego.NewRectFromPoints(
		rtreego.Point{s.Position.Lat, s.Position.Lon},
		rtreego.Point{s.Position.Lat, s.Position.Lon},
	)
	if err != nil {
		panic(err.Error())
	}
	return rect
}
