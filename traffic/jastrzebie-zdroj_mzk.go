// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package traffic

import (
	"apiote.xyz/p/szczanieckiej/transformers"

	"net/http"
	"time"

	"golang.org/x/text/transform"
)

type JaztrzebieZdrojMZK struct {
	client http.Client
}

func (JaztrzebieZdrojMZK) getTimezone() *time.Location {
	l, _ := time.LoadLocation("Europe/Warsaw")
	return l
}

func (g JaztrzebieZdrojMZK) ConvertVehicles() ([]Vehicle, error) {
	return []Vehicle{}, nil
}

func (z JaztrzebieZdrojMZK) GetVersions(_ time.Time, timezone *time.Location) ([]Version, error) {
	version, err := MakeVersionTimezone("00010101_99991231", timezone)
	if err != nil {
		return nil, err
	}
	version.Link = "https://bimba.app/gtfs/pl-24_2467.zip"
	return []Version{version}, nil
}

func (JaztrzebieZdrojMZK) String() string {
	return "jastrzebie-zdroj_mzk"
}

func (JaztrzebieZdrojMZK) RealtimeFeeds() map[RealtimeFeedType]string {
	return map[RealtimeFeedType]string{}
}

func (JaztrzebieZdrojMZK) Transformer() transform.Transformer {
	return transformers.TransformerPL
}

func (JaztrzebieZdrojMZK) Name() string {
	return "Jastrzębie-Zdrój MZK"
}

func (JaztrzebieZdrojMZK) Flags() FeedFlags {
	return FeedFlags{
		Headsign:     HeadsignTripHeadsing,
		StopIdFormat: "{{stop_id}}",
		StopName:     "{{stop_name}}",
		LineName:     "{{route_short_name}}",
	}
}

func (JaztrzebieZdrojMZK) FeedPrepareZip(path string) error {
	return nil
}

func (JaztrzebieZdrojMZK) QRInfo() (string, QRLocation, string) {
	return "", QRLocationNone, ""
}
