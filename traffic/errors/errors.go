// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package errors

import (
	"fmt"
)

type NoSchedule struct {
	Date string
}

func (e NoSchedule) Error() string {
	return "No schedule found for " + e.Date
}

type NoStopOrder struct {
	TripID string
	Order  int
}

func (e NoStopOrder) Error() string {
	return fmt.Sprintf("No stopOrder for trip %s, order %d", e.TripID, e.Order)
}

type VersionError struct {
	Msg string
}

func (v VersionError) Error() string {
	return v.Msg
}

type NoVersionError struct {
	Date string
}

func (e NoVersionError) Error() string {
	return "No version for " + e.Date
}
