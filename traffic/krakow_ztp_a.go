// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package traffic

import (
	"apiote.xyz/p/szczanieckiej/transformers"

	"time"

	"golang.org/x/text/transform"
)

type KrakowZtpA struct{}

func (KrakowZtpA) getTimezone() *time.Location {
	l, _ := time.LoadLocation("Europe/Warsaw")
	return l
}

func (KrakowZtpA) ConvertVehicles() ([]Vehicle, error) {
	return []Vehicle{}, nil
}

func (f KrakowZtpA) GetVersions(date time.Time, timezone *time.Location) ([]Version, error) {
	startDate := time.Date(date.Year(), date.Month(), date.Day(), 0, 0, 0, 0, timezone)
	endDate := time.Date(date.Year(), date.Month(), date.Day(), 23, 59, 59, 0, timezone)
	v := Version{
		Link:      "https://gtfs.ztp.krakow.pl/GTFS_KRK_A.zip",
		ValidFrom: startDate,
		ValidTill: endDate,
	}
	return []Version{v}, nil
}

func (KrakowZtpA) RealtimeFeeds() map[RealtimeFeedType]string {
	return map[RealtimeFeedType]string{
		ALERTS:            "https://gtfs.ztp.krakow.pl/ServiceAlerts_A.pb",
		TRIP_UPDATES:      "https://gtfs.ztp.krakow.pl/TripUpdates_A.pb",
		VEHICLE_POSITIONS: "https://gtfs.ztp.krakow.pl/VehiclePositions_A.pb",
	}
}

func (KrakowZtpA) String() string {
	return "krakow_ztp_a"
}

func (z KrakowZtpA) Transformer() transform.Transformer {
	return transformers.TransformerPL
}

func (z KrakowZtpA) Name() string {
	return "ZTP Kraków Bus"
}

func (z KrakowZtpA) Flags() FeedFlags {
	return FeedFlags{
		Headsign:     HeadsignTripHeadsing,
		StopIdFormat: "{{stop_id}}",
		StopName:     "{{stop_name}}",
		LineName:     "{{route_short_name}}",
	}
}

func (z KrakowZtpA) FeedPrepareZip(path string) error {
	return nil
}

func (z KrakowZtpA) QRInfo() (string, QRLocation, string) {
	return "", QRLocationNone, ""
}
