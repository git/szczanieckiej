// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package traffic

import (
	"apiote.xyz/p/szczanieckiej/config"

	"bytes"
	"log"
	"strings"
	"testing"
	"text/template"

	"github.com/yuin/gopher-lua"
)

func TestLuaScripts(t *testing.T) {
	for feedID := range RegisterFeeds() {
		filenames := []string{"updates", "vehicles", "alerts"}
		for _, filename := range filenames {
			var buffer bytes.Buffer
			temp, err := template.ParseFS(luaScripts, feedID+"_"+filename+".lua")
			if err != nil {
				if strings.Contains(err.Error(), "pattern matches no files") {
					log.Printf("%s.lua for %s does not exist, ignoring\n", feedID, filename)
					continue
				}
				t.Errorf("while parsing template %s: %v", filename, err)
				continue
			}
			err = temp.Execute(&buffer, config.Auth{})
			if err != nil {
				t.Errorf("while executing template %s: %v", filename, err)
				continue
			}
			updateScript := string(buffer.Bytes())
			l := lua.NewState()
			if err := l.DoString(updateScript); err != nil {
				t.Errorf("in updates, in %s: %v\n", feedID, err)
			}
			l.Close()
		}
	}
}
