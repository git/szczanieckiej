// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package traffic

import (
	"apiote.xyz/p/szczanieckiej/transformers"

	"time"

	"golang.org/x/text/transform"
)

type KrakowZtpM struct{}

func (KrakowZtpM) getTimezone() *time.Location {
	l, _ := time.LoadLocation("Europe/Warsaw")
	return l
}

func (KrakowZtpM) ConvertVehicles() ([]Vehicle, error) {
	return []Vehicle{}, nil
}

func (f KrakowZtpM) GetVersions(date time.Time, timezone *time.Location) ([]Version, error) {
	startDate := time.Date(date.Year(), date.Month(), date.Day(), 0, 0, 0, 0, timezone)
	endDate := time.Date(date.Year(), date.Month(), date.Day(), 23, 59, 59, 0, timezone)
	v := Version{
		Link:      "https://gtfs.ztp.krakow.pl/GTFS_KRK_M.zip",
		ValidFrom: startDate,
		ValidTill: endDate,
	}
	return []Version{v}, nil
}

func (KrakowZtpM) RealtimeFeeds() map[RealtimeFeedType]string {
	return map[RealtimeFeedType]string{
		ALERTS:            "https://gtfs.ztp.krakow.pl/ServiceAlerts_M.pb",
		TRIP_UPDATES:      "https://gtfs.ztp.krakow.pl/TripUpdates_M.pb",
		VEHICLE_POSITIONS: "https://gtfs.ztp.krakow.pl/VehiclePositions_M.pb",
	}
}

func (KrakowZtpM) String() string {
	return "krakow_ztp_m"
}

func (z KrakowZtpM) Transformer() transform.Transformer {
	return transformers.TransformerPL
}

func (z KrakowZtpM) Name() string {
	return "ZTP Kraków Mobilis"
}

func (z KrakowZtpM) Flags() FeedFlags {
	return FeedFlags{
		Headsign:     HeadsignTripHeadsing,
		StopIdFormat: "{{stop_id}}",
		StopName:     "{{stop_name}}",
		LineName:     "{{route_short_name}}",
	}
}

func (z KrakowZtpM) FeedPrepareZip(path string) error {
	return nil
}

func (z KrakowZtpM) QRInfo() (string, QRLocation, string) {
	return "", QRLocationNone, ""
}
