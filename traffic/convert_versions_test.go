// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package traffic

import (
	"testing"
	"time"
)

func TestFindValidVersionsDecember2023Poznan(t *testing.T) {
	timezone, _ := time.LoadLocation("Europe/Warsaw")
	validities := []Validity{
		"20231201_20231203",
		"20231202_20231222",
		"20231209_20231215",
		"20231214_20231215",
		"20231215_20231215",
		"20231216_20231222",
		"20231223_20231224",
		"20231225_20231225",
		"20231226_20231230",
	}
	allVersions := make([]Version, len(validities))
	var err error
	for i, validity := range validities {
		allVersions[i], err = MakeVersionTimezone(string(validity), timezone)
		if err != nil {
			t.Fatalf("MakeVersion errored on %s: %v\n", validity, err)
		}
	}
	validVersions := FindValidVersions(allVersions, time.Date(2023, time.December, 1, 4, 0, 0, 0, timezone))
	if len(validVersions) != 9 {
		t.Fatalf("wanted 9 elements, got %v\n", validVersions)
	}
}

func TestFindValidVersionsChristmasEve2023Poznan(t *testing.T) {
	timezone, _ := time.LoadLocation("Europe/Warsaw")
	validities := []Validity{
		"20231223_20231224",
		"20231225_20231225",
		"20231226_20231230",
	}
	allVersions := make([]Version, len(validities))
	var err error
	for i, validity := range validities {
		allVersions[i], err = MakeVersionTimezone(string(validity), timezone)
		if err != nil {
			t.Fatalf("MakeVersion errored on %s: %v\n", validity, err)
		}
	}
	validVersions := FindValidVersions(allVersions, time.Date(2023, time.December, 24, 4, 0, 0, 0, timezone))
	if len(validVersions) != 3 && validVersions[0].String() == "20231223_20231224" && validVersions[1].String() == "20231225_20231225" && validVersions[2].String() == "20231226_20231230" {
		t.Fatalf("wanted 3 elements, got %v\n", validVersions)
	}
}

func TestFindValidVersionsChristmasDay2023Poznan(t *testing.T) {
	timezone, _ := time.LoadLocation("Europe/Warsaw")
	validities := []Validity{
		"20231223_20231224",
		"20231225_20231225",
		"20231226_20231230",
	}
	allVersions := make([]Version, len(validities))
	var err error
	for i, validity := range validities {
		allVersions[i], err = MakeVersionTimezone(string(validity), timezone)
		if err != nil {
			t.Fatalf("MakeVersion errored on %s: %v\n", validity, err)
		}
	}
	validVersions := FindValidVersions(allVersions, time.Date(2023, time.December, 25, 4, 0, 0, 0, timezone))
	if len(validVersions) != 2 && validVersions[0].String() == "20231225_20231225" && validVersions[1].String() == "20231226_20231230" {
		t.Fatalf("wanted 2 elements, got %v\n", validVersions)
	}
}

func TestFindValidVersionsBoxingDay2023Poznan(t *testing.T) {
	timezone, _ := time.LoadLocation("Europe/Warsaw")
	validities := []Validity{
		"20231223_20231224",
		"20231225_20231225",
		"20231226_20231230",
	}
	allVersions := make([]Version, len(validities))
	var err error
	for i, validity := range validities {
		allVersions[i], err = MakeVersionTimezone(string(validity), timezone)
		if err != nil {
			t.Fatalf("MakeVersion errored on %s: %v\n", validity, err)
		}
	}
	validVersions := FindValidVersions(allVersions, time.Date(2023, time.December, 26, 4, 0, 0, 0, timezone))
	if len(validVersions) != 1 && validVersions[0].String() == "20231226_20231230" {
		t.Fatalf("wanted 1 elements, got %v\n", validVersions)
	}
}

func TestFindValidVersionsSingleDay(t *testing.T) {
	timezone, _ := time.LoadLocation("Europe/Warsaw")
	validities := []Validity{
		"20231225_20231225",
	}
	allVersions := make([]Version, len(validities))
	var err error
	for i, validity := range validities {
		allVersions[i], err = MakeVersionTimezone(string(validity), timezone)
		if err != nil {
			t.Fatalf("MakeVersion errored on %s: %v\n", validity, err)
		}
	}
	validVersions := FindValidVersions(allVersions, time.Date(2023, time.December, 25, 4, 0, 0, 0, timezone))
	if len(validVersions) != 1 && validVersions[0].String() == "20231225_20231225" {
		t.Fatalf("wanted 1 elements, got %v\n", validVersions)
	}
}
