// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package traffic

import (
	"apiote.xyz/p/szczanieckiej/config"

	"io"
	"os"
	"path/filepath"
)

func GetTimetable(feedID string, t *Traffic, c config.Config) (io.ReadCloser, error) {
	validity, _, err := ParseDate("", feedID, t)
	path := filepath.Join(c.FeedsPath, feedID, string(validity)+".txz")
	f, err := os.Open(path)
	return f, err
}
