// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package traffic

import (
	"apiote.xyz/p/szczanieckiej/transformers"

	"net/http"
	"time"

	"golang.org/x/text/transform"
)

type SkpBana struct {
	client http.Client
}

func (SkpBana) getTimezone() *time.Location {
	l, _ := time.LoadLocation("Europe/Warsaw")
	return l
}

func (g SkpBana) ConvertVehicles() ([]Vehicle, error) {
	return []Vehicle{}, nil
}

func (z SkpBana) GetVersions(_ time.Time, timezone *time.Location) ([]Version, error) {
	version, err := MakeVersionTimezone("00010101_99991231", timezone)
	if err != nil {
		return nil, err
	}
	version.Link = "https://bimba.app/gtfs/pl-30_3025_bana.zip"
	return []Version{version}, nil
}

func (SkpBana) String() string {
	return "skp_bana"
}

func (SkpBana) RealtimeFeeds() map[RealtimeFeedType]string {
	return map[RealtimeFeedType]string{}
}

func (SkpBana) Transformer() transform.Transformer {
	return transformers.TransformerPL
}

func (SkpBana) Name() string {
	return "Średzka Kolej Powiatowa"
}

func (SkpBana) Flags() FeedFlags {
	return FeedFlags{
		Headsign:     HeadsignTripHeadsing,
		StopIdFormat: "{{stop_id}}",
		StopName:     "{{stop_name}}",
		LineName:     "{{route_short_name}}",
	}
}

func (SkpBana) FeedPrepareZip(path string) error {
	return nil
}

func (SkpBana) QRInfo() (string, QRLocation, string) {
	return "", QRLocationNone, ""
}
