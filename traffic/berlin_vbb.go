// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package traffic

import (
	"apiote.xyz/p/szczanieckiej/transformers"

	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"golang.org/x/text/transform"
)

type BerlinVbb struct {
	client http.Client
}

func (BerlinVbb) getTimezone() *time.Location {
	l, _ := time.LoadLocation("Europe/Berlin")
	return l
}

func (BerlinVbb) ConvertVehicles() ([]Vehicle, error) {
	return []Vehicle{}, nil
}

func (BerlinVbb) GetVersions(date time.Time, timezone *time.Location) ([]Version, error) {
	versions := []Version{}
	version, err := MakeVersionTimezone("00010101_99991231", timezone)
	if err != nil {
		return nil, err
	}
	version.Link = "https://www.vbb.de/fileadmin/user_upload/VBB/Dokumente/API-Datensaetze/gtfs-mastscharf/GTFS.zip"
	versions = append(versions, version)
	return versions, nil
}

func (BerlinVbb) String() string {
	return "berlin_vbb"
}

func (BerlinVbb) RealtimeFeeds() map[RealtimeFeedType]string {
	return map[RealtimeFeedType]string{}
}

func (BerlinVbb) Transformer() transform.Transformer {
	return transform.Chain(transformers.TransformerDE, transformers.TransformerPL, transformers.TransformerFR)
}

func (BerlinVbb) Name() string {
	return "Berlin/Brandenburg VBB"
}

func (BerlinVbb) Flags() FeedFlags {
	return FeedFlags{
		Headsign:     HeadsignTripHeadsing,
		StopIdFormat: "{{stop_id}}_{{platform_code}}",
		StopName:     "{{stop_name}} | {{platform_code}}", // TODO platform_code might not be present, but currently templating doesn’t support this
		LineName:     "{{route_short_name}}",
	}
}

func (BerlinVbb) FeedPrepareZip(path string) error {
	// TODO remove or fix zones

	// NOTE add non-printable characrer before umlauts
	trips2File, err := os.OpenFile(filepath.Join(path, "trips2.txt"), os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		return fmt.Errorf("while opening trips2 file: %w", err)
	}
	defer trips2File.Close()
	w := csv.NewWriter(trips2File)
	err = forEachRowWithHeader(filepath.Join(path, "trips.txt"), func(header []string) error {
		err := w.Write(header)
		if err != nil {
			return fmt.Errorf("while writing header: %w", err)
		} else {
			return nil
		}
	}, func(offset int64, fields map[string]int, record []string) error {
		record[fields["trip_headsign"]] = strings.ReplaceAll(record[fields["trip_headsign"]], "ü", ZWJ+"ü")
		record[fields["trip_headsign"]] = strings.ReplaceAll(record[fields["trip_headsign"]], "Ü", ZWJ+"Ü")
		record[fields["trip_headsign"]] = strings.ReplaceAll(record[fields["trip_headsign"]], "ä", ZWJ+"ä")
		record[fields["trip_headsign"]] = strings.ReplaceAll(record[fields["trip_headsign"]], "Ä", ZWJ+"Ä")
		record[fields["trip_headsign"]] = strings.ReplaceAll(record[fields["trip_headsign"]], "ö", ZWJ+"ö")
		record[fields["trip_headsign"]] = strings.ReplaceAll(record[fields["trip_headsign"]], "Ö", ZWJ+"Ö")

		err := w.Write(record)
		if err != nil {
			return fmt.Errorf("while writing record: %w", err)
		} else {
			return nil
		}
	})
	w.Flush()
	err = os.Remove(filepath.Join(path, "trips.txt"))
	if err != nil {
		return fmt.Errorf("while removing trips: %w", err)
	}
	err = os.Rename(filepath.Join(path, "trips2.txt"), filepath.Join(path, "trips.txt"))
	if err != nil {
		return fmt.Errorf("while renaming trips: %w", err)
	}

	// NOTE add platform codes from stop_id and add nonprintable character before umlauts
	stopsFile, err := os.Open(filepath.Join(path, "stops.txt"))
	if err != nil {
		return fmt.Errorf("while opening stops file: %w", err)
	}
	defer stopsFile.Close()
	stops2File, err := os.OpenFile(filepath.Join(path, "stops2.txt"), os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		return fmt.Errorf("while opening stops2 file: %w", err)
	}
	defer stops2File.Close()
	r := csv.NewReader(bufio.NewReader(stopsFile))
	w = csv.NewWriter(stops2File)
	header, err := r.Read()
	if err != nil {
		return fmt.Errorf("while reading stops header: %w", err)
	}
	fields := map[string]int{}
	for i, headerField := range header {
		fields[headerField] = i
	}
	err = w.Write(header)
	if err != nil {
		return fmt.Errorf("while writing stops header: %w", err)
	}
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return fmt.Errorf("while reading a stop record: %w", err)
		}
		record[fields["stop_name"]] = strings.ReplaceAll(record[fields["stop_name"]], "ü", ZWJ+"ü")
		record[fields["stop_name"]] = strings.ReplaceAll(record[fields["stop_name"]], "Ü", ZWJ+"Ü")
		record[fields["stop_name"]] = strings.ReplaceAll(record[fields["stop_name"]], "ä", ZWJ+"ä")
		record[fields["stop_name"]] = strings.ReplaceAll(record[fields["stop_name"]], "Ä", ZWJ+"Ä")
		record[fields["stop_name"]] = strings.ReplaceAll(record[fields["stop_name"]], "ö", ZWJ+"ö")
		record[fields["stop_name"]] = strings.ReplaceAll(record[fields["stop_name"]], "Ö", ZWJ+"Ö")

		f, ok := fields["location_type"]
		if (!ok || record[f] == "" || record[f] == "0") && record[fields["platform_code"]] == "" {
			stopID := strings.Split(record[fields["stop_id"]], ":")
			if len(stopID) >= 5 {
				record[fields["platform_code"]] = stopID[4]
			}
		}
		err = w.Write(record)
		if err != nil {
			return fmt.Errorf("while writing a stop record: %w", err)
		}
	}
	w.Flush()
	err = os.Remove(filepath.Join(path, "stops.txt"))
	if err != nil {
		return fmt.Errorf("while removing stops: %w", err)
	}
	err = os.Rename(filepath.Join(path, "stops2.txt"), filepath.Join(path, "stops.txt"))
	if err != nil {
		return fmt.Errorf("while renaming stops: %w", err)
	}

	// NOTE fix route types and add nonprintable character before umlauts
	routesFile, err := os.Open(filepath.Join(path, "routes.txt"))
	if err != nil {
		return fmt.Errorf("while opening routes file: %w", err)
	}
	defer routesFile.Close()
	routes2File, err := os.OpenFile(filepath.Join(path, "routes2.txt"), os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		return fmt.Errorf("while opening routes2 file: %w", err)
	}
	defer routes2File.Close()
	r = csv.NewReader(bufio.NewReader(routesFile))
	w = csv.NewWriter(routes2File)
	header, err = r.Read()
	if err != nil {
		return fmt.Errorf("while reading routes header: %w", err)
	}
	fields = map[string]int{}
	for i, headerField := range header {
		fields[headerField] = i
	}
	err = w.Write(header)
	if err != nil {
		return fmt.Errorf("while writing routes header: %w", err)
	}

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return fmt.Errorf("while reading a route record: %w", err)
		}
		record[fields["route_long_name"]] = strings.ReplaceAll(record[fields["route_long_name"]], "ü", ZWJ+"ü")
		record[fields["route_short_name"]] = strings.ReplaceAll(record[fields["route_short_name"]], "ü", ZWJ+"ü")
		record[fields["route_long_name"]] = strings.ReplaceAll(record[fields["route_long_name"]], "Ü", ZWJ+"Ü")
		record[fields["route_short_name"]] = strings.ReplaceAll(record[fields["route_short_name"]], "Ü", ZWJ+"Ü")
		record[fields["route_long_name"]] = strings.ReplaceAll(record[fields["route_long_name"]], "ä", ZWJ+"ä")
		record[fields["route_short_name"]] = strings.ReplaceAll(record[fields["route_short_name"]], "ä", ZWJ+"ä")
		record[fields["route_long_name"]] = strings.ReplaceAll(record[fields["route_long_name"]], "Ä", ZWJ+"Ä")
		record[fields["route_short_name"]] = strings.ReplaceAll(record[fields["route_short_name"]], "Ä", ZWJ+"Ä")
		record[fields["route_long_name"]] = strings.ReplaceAll(record[fields["route_long_name"]], "ö", ZWJ+"ö")
		record[fields["route_short_name"]] = strings.ReplaceAll(record[fields["route_short_name"]], "ö", ZWJ+"ö")
		record[fields["route_long_name"]] = strings.ReplaceAll(record[fields["route_long_name"]], "Ö", ZWJ+"Ö")
		record[fields["route_short_name"]] = strings.ReplaceAll(record[fields["route_short_name"]], "Ö", ZWJ+"Ö")

		if record[fields["route_type"]] == "100" {
			record[fields["route_type"]] = "2"
		}
		if record[fields["route_type"]] == "109" {
			record[fields["route_type"]] = "1"
		}
		if record[fields["route_type"]] == "400" {
			record[fields["route_type"]] = "1"
		}
		if record[fields["route_type"]] == "700" {
			record[fields["route_type"]] = "3"
		}
		if record[fields["route_type"]] == "900" {
			record[fields["route_type"]] = "0"
		}
		if record[fields["route_type"]] == "1000" {
			record[fields["route_type"]] = "4"
		}
		err = w.Write(record)
		if err != nil {
			return fmt.Errorf("while writing a route record: %w", err)
		}
	}
	w.Flush()
	err = os.Remove(filepath.Join(path, "routes.txt"))
	if err != nil {
		return fmt.Errorf("while removing routes: %w", err)
	}
	err = os.Rename(filepath.Join(path, "routes2.txt"), filepath.Join(path, "routes.txt"))
	if err != nil {
		return fmt.Errorf("while renaming routes: %w", err)
	}
	return nil
}

func (BerlinVbb) QRInfo() (string, QRLocation, string) {
	// TODO mapping IDs
	return "qr.bvg.de", QRLocationPath, "/(?<SEL>.*)"
}
