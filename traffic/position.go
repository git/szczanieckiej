// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package traffic

import (
	"fmt"
	"strconv"
	"strings"
)

func ParsePosition(location string) (Position, error) {
	locationString := strings.Split(location, ",")
	if len(locationString) != 2 {
		return Position{}, fmt.Errorf("location is not two numbers")
	}
	lat, err := strconv.ParseFloat(locationString[0], 64)
	if err != nil {
		return Position{}, fmt.Errorf("latitude is not a float")
	}
	lon, err := strconv.ParseFloat(locationString[1], 64)
	if err != nil {
		return Position{}, fmt.Errorf("longitude is not a float")
	}
	return Position{Lat: lat, Lon: lon}, nil
}
