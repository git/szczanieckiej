// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

module apiote.xyz/p/szczanieckiej

go 1.22.0

toolchain go1.23.2

require (
	apiote.xyz/p/gott/v2 v2.0.3
	git.sr.ht/~sircmpwn/go-bare v0.0.0-20210406120253-ab86bc2846d9
	github.com/BurntSushi/toml v1.3.2
	github.com/JohannesKaufmann/html-to-markdown v1.5.0
	github.com/adrg/strutil v0.3.0
	github.com/cjoudrey/gluahttp v0.0.0-20201111170219-25003d9adfa9
	github.com/dhconnelly/rtreego v1.1.0
	github.com/lanrat/extsort v1.0.2
	github.com/mattn/go-sqlite3 v1.14.24
	github.com/sahilm/fuzzy v0.1.0
	github.com/ulikunitz/xz v0.5.10
	github.com/yuin/gopher-lua v1.1.1
	golang.org/x/text v0.19.0
	golang.org/x/tools v0.26.0
	google.golang.org/protobuf v1.26.0
	gopkg.in/yaml.v3 v3.0.1
	layeh.com/gopher-json v0.0.0-20201124131017-552bb3c4c3bf
	notabug.org/apiote/gott v1.1.2
)

require (
	github.com/PuerkitoBio/goquery v1.8.1 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/kylelemons/godebug v1.1.0 // indirect
	github.com/stretchr/testify v1.8.0 // indirect
	golang.org/x/net v0.30.0 // indirect
	golang.org/x/sync v0.8.0 // indirect
)
