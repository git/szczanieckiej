// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package transformers

import (
	"golang.org/x/text/transform"
)

//nolint:gochecknoglobals
var TransformerFY transform.Transformer = Replace(func(r rune) []rune {
	switch r {
	case 'â':
		return []rune{'a'}
	case 'Â':
		return []rune{'A'}
	case 'ê':
		return []rune{'e'}
	case 'Ê':
		return []rune{'E'}
	case 'é':
		return []rune{'e'}
	case 'É':
		return []rune{'E'}
	case 'ô':
		return []rune{'o'}
	case 'Ô':
		return []rune{'O'}
	case 'û':
		return []rune{'u'}
	case 'Û':
		return []rune{'U'}
	case 'ú':
		return []rune{'u'}
	case 'Ú':
		return []rune{'U'}
	default:
		return []rune{r}
	}
})
