// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package transformers

import (
	"golang.org/x/text/transform"
)

//nolint:gochecknoglobals
var TransformerFR transform.Transformer = Replace(func(r rune) []rune {
	switch r {
	case 'à':
		return []rune{'a'}
	case 'À':
		return []rune{'A'}
	case 'â':
		return []rune{'a'}
	case 'Â':
		return []rune{'A'}
	case 'æ':
		return []rune{'a', 'e'}
	case 'Æ':
		return []rune{'A', 'E'}
	case 'ç':
		return []rune{'c'}
	case 'Ç':
		return []rune{'C'}
	case 'è':
		return []rune{'e'}
	case 'È':
		return []rune{'E'}
	case 'é':
		return []rune{'e'}
	case 'É':
		return []rune{'E'}
	case 'ë':
		return []rune{'e'}
	case 'Ë':
		return []rune{'E'}
	case 'ê':
		return []rune{'e'}
	case 'Ê':
		return []rune{'E'}
	case 'î':
		return []rune{'i'}
	case 'Î':
		return []rune{'I'}
	case 'ï':
		return []rune{'i'}
	case 'Ï':
		return []rune{'I'}
	case 'ô':
		return []rune{'o'}
	case 'Ô':
		return []rune{'O'}
	case 'œ':
		return []rune{'o', 'e'}
	case 'Œ':
		return []rune{'O', 'E'}
	case 'û':
		return []rune{'u'}
	case 'Û':
		return []rune{'U'}
	case 'ù':
		return []rune{'u'}
	case 'Ù':
		return []rune{'U'}
	case 'ú':
		return []rune{'u'}
	case 'Ú':
		return []rune{'U'}
	case 'ÿ':
		return []rune{'y'}
	case 'Ÿ':
		return []rune{'Y'}
	default:
		return []rune{r}
	}
})
