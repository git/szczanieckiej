// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package transformers

import (
	"golang.org/x/text/transform"
)

//nolint:gochecknoglobals
var TransformerSV transform.Transformer = ContextualLatiniser{
	Replace: func(previous, r rune) []rune {
		switch r {
		case 'ö':
			if previous == ZWJ {
				return []rune{'o', 'e'}
			}
			return []rune{r}
		case 'Ö':
			if previous == ZWJ {
				return []rune{'O', 'e'}
			}
			return []rune{r}
		case 'ä':
			if previous == ZWJ {
				return []rune{'a', 'e'}
			}
			return []rune{r}
		case 'Ä':
			if previous == ZWJ {
				return []rune{'A', 'e'}
			}
			return []rune{r}
		case 'å':
			return []rune{'a', 'a'}
		case 'Å':
			return []rune{'A', 'a'}
		default:
			return []rune{r}
		}
	},
}
