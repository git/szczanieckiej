// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package transformers

import (
	"golang.org/x/text/transform"
)

//nolint:gochecknoglobals
var TransformerNL transform.Transformer = ContextualLatiniser{
	Replace: func(previous, r rune) []rune {
		switch r {
		case 'á':
			return []rune{'a'}
		case 'Á':
			return []rune{'A'}
		case 'ĳ':
			return []rune{'i', 'j'}
		case 'Ĳ':
			return []rune{'I', 'J'}
		case 'ä':
			if previous != ZWJ {
				return []rune{'a'}
			}
			return []rune{r}
		case 'Ä':
			if previous != ZWJ {
				return []rune{'A'}
			}
			return []rune{r}
		case 'ë':
			return []rune{'e'}
		case 'Ë':
			return []rune{'E'}
		case 'ï':
			return []rune{'i'}
		case 'Ï':
			return []rune{r}
		case 'ö':
			if previous != ZWJ {
				return []rune{'o'}
			}
			return []rune{r}
		case 'Ö':
			if previous != ZWJ {
				return []rune{'O'}
			}
			return []rune{r}
		case 'ü':
			if previous != ZWJ {
				return []rune{'u'}
			}
			return []rune{r}
		case 'Ü':
			if previous != ZWJ {
				return []rune{'U'}
			}
			return []rune{r}
		default:
			return []rune{r}
		}
	},
}
