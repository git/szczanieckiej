// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package transformers

import (
	"golang.org/x/text/transform"
)

//nolint:gochecknoglobals
var TransformerDA transform.Transformer = Replace(func(r rune) []rune {
	switch r {
	case 'æ':
		return []rune{'a', 'e'}
	case 'Æ':
		return []rune{'A', 'E'}
	case 'å':
		return []rune{'a', 'a'}
	case 'Å':
		return []rune{'A', 'A'}
	case 'ø':
		return []rune{'o', 'e'}
	case 'Ø':
		return []rune{'O', 'E'}
	default:
		return []rune{r}
	}
})
