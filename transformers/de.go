// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package transformers

import (
	"golang.org/x/text/transform"
)

//nolint:gochecknoglobals
var TransformerDE transform.Transformer = ContextualLatiniser{
	Replace: func(previous, r rune) []rune {
		switch r {
		case 'ö':
			if previous == ZWJ {
				return []rune{'o', 'e'}
			}
			return []rune{r}
		case 'Ö':
			if previous == ZWJ {
				return []rune{'O', 'e'}
			}
			return []rune{r}
		case 'ä':
			if previous == ZWJ {
				return []rune{'a', 'e'}
			}
			return []rune{r}
		case 'Ä':
			if previous == ZWJ {
				return []rune{'A', 'e'}
			}
			return []rune{r}
		case 'ß':
			if previous == ZWJ {
				return []rune{'s', 's'}
			}
			return []rune{r}
		case 'ẞ':
			if previous == ZWJ {
				return []rune{'S', 'S'}
			}
			return []rune{r}
		case 'ü':
			if previous == ZWJ {
				return []rune{'u', 'e'}
			}
			return []rune{r}
		case 'Ü':
			if previous == ZWJ {
				return []rune{'U', 'e'}
			}
			return []rune{r}
		default:
			return []rune{r}
		}
	},
}
