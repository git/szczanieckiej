// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package transformers

import (
	"golang.org/x/text/transform"
)

//nolint:gochecknoglobals
var TransformerPL transform.Transformer = Replace(func(r rune) []rune {
	switch r {
	case 'ę':
		return []rune{'e'}
	case 'Ę':
		return []rune{'E'}
	case 'ó':
		return []rune{'o'}
	case 'Ó':
		return []rune{'O'}
	case 'ą':
		return []rune{'a'}
	case 'Ą':
		return []rune{'A'}
	case 'ś':
		return []rune{'s'}
	case 'Ś':
		return []rune{'S'}
	case 'ł':
		return []rune{'l'}
	case 'Ł':
		return []rune{'L'}
	case 'ż':
		return []rune{'z'}
	case 'Ż':
		return []rune{'Z'}
	case 'ź':
		return []rune{'z'}
	case 'Ź':
		return []rune{'Z'}
	case 'ć':
		return []rune{'c'}
	case 'Ć':
		return []rune{'C'}
	case 'ń':
		return []rune{'n'}
	case 'Ń':
		return []rune{'N'}
	default:
		return []rune{r}
	}
})
