# SPDX-FileCopyrightText: Adam Evyčędo
#
# SPDX-License-Identifier: AGPL-3.0-or-later

ALL=`echo api/* config/* file/* gtfs_rt/* main.go server/* traffic/* transformers/*`

all:V: szczanieckiej

init:V: api/structs_gen.go traffic/structs_gen.go

lint:V:
	golangci-lint -j 2 run --fast --timeout 30m --skip-files '(gtfs-realtime.pb.go|structs_gen.go)' -E asasalint -E asciicheck -E bidichk -E bodyclose -E contextcheck -E decorder -E dupl -E durationcheck -E errcheck -E errname -E errorlint -E exhaustive -E exhaustruct -E exportloopref -E gocritic -E gofmt -E goimports -E gofumpt -E gomnd -E gomoddirectives -E gosec -E gosimple -E govet -E importas -E ineffassign -E interfacer -E ireturn -E loggercheck -E misspell -E nilerr -E nilnil -E noctx -E nolintlint -E prealloc -E predeclared -E reassign -E revive -E staticcheck -E stylecheck -E tagliatelle -E unconvert -E unparam -E unused -E usestdlibvars -E wastedassign -E wrapcheck -D containedctx -D cyclop -D deadcode -D depguard -D dogsled -D dupword -D errchkjson -D execinquery -D exhaustivestruct -D forbidigo -D forcetypeassert -D funlen -D gci -D ginkgolinter -D gocheckcompilerdirectives -D gochecknoglobals -D gochecknoinits -D gocognit -D goconst -D gocyclo -D godox -D goerr113 -D goheader -D golint -D gomodguard -D goprintffuncname -D grouper -D ifshort -D interfacebloat -D lll -D maintidx -D makezero -D maligned -D musttag -D nestif -D nlreturn -D nonamedreturns -D nosnakecase -D nosprintfhostport -D paralleltest -D promlinter -D testpackage -D typecheck -D varnamelen -D whitespace -D wsl

test:V: $ALL
	reuse lint
	go test ./...

szczanieckiej: init $ALL test
	go build

api/structs_gen.go: ../traffic/api/api.bare
	bare-gen -p api ../traffic/api/api.bare api/structs_gen2.go
	awk '!seen[$0]++ || $0 !~ /func \(_ StopV1\) IsUnion\(\) \{\}/' api/structs_gen2.go >api/structs_gen3.go
	awk '!seen[$0]++ || $0 !~ /func \(_ StopV2\) IsUnion\(\) \{\}/' api/structs_gen3.go >api/structs_gen.go
	rm api/structs_gen2.go api/structs_gen3.go

traffic/structs_gen.go: ../traffic/traffic.bare
	bare-gen -p traffic ../traffic/traffic.bare traffic/structs_gen.go
