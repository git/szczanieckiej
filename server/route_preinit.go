// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package server

import (
	"errors"

	"apiote.xyz/p/szczanieckiej/config"

	"log"
	"net/http"
	"os"
)

func RoutePreinit(cfg config.Config) *http.Server {
	srv := &http.Server{Addr: cfg.ListenAddress}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		se := ServerError{
			code: http.StatusServiceUnavailable,
			err:  errors.New("not yet initialised"),
		}
		sendError(w, r, se)
	})

	go func() {
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			log.Printf("Preinit ListenAndServe(): %v", err)
			os.Exit(1)
		}
	}()
	return srv
}
